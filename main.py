from config import url as source
from config import file_path, encoding
from city import My_city, Person

import requests
from fake_useragent import UserAgent
import csv

def main():
    response = requests.get(
        url = source,
        headers = {'User-Agent': UserAgent().chrome}
        )
    raw_data = str(response.text).split('Наша команда')[1].split('Вакансии')[0].split("n_text_1596441248760'>")
    raw_data.pop(0)
    cities = raw_data
    persons = []
    for c in cities:
        city = My_city(c)
        person = city.get_left_person()
        if person.name != '':
            persons.append(person)
    with open(file_path + '.csv', mode="w", encoding = encoding) as output_file:
        output = csv.writer(output_file, delimiter = ',', lineterminator="\r")
        output.writerow(['Имя','Город','Должность','Почта'])
        for person in persons:
            output.writerow([person.name, person.city, person.post, person.email])

if __name__ == '__main__':
    main()
